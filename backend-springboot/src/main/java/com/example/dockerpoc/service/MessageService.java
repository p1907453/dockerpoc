package com.example.dockerpoc.service;

import com.example.dockerpoc.dao.MessageDao;
import com.example.dockerpoc.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    @Autowired
    private MessageDao messageDao;
    public List<Message> getMessages() {
        return messageDao.findAll();
    }

    public Message getMessageById(int id) {
        return messageDao.findById(id).orElse(null);
    }

    public Message addMessage(Message message) {
        return messageDao.save(message);
    }

    public void deleteMessage(int id) {
        messageDao.deleteById(id);
    }

}

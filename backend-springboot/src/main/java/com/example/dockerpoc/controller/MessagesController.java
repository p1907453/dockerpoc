package com.example.dockerpoc.controller;

import com.example.dockerpoc.model.Message;
import com.example.dockerpoc.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessagesController {
    @Autowired
    private MessageService messageService;

    @GetMapping("/messages")
    public List<Message> getMessages() {
        return messageService.getMessages();
    }

    @GetMapping("/message/{id}")
    public Message getMessageById(@PathVariable int id) {
        return messageService.getMessageById(id);
    }

    @DeleteMapping("/message/{id}")
    public void deleteMessage(@PathVariable int id) {
        messageService.deleteMessage(id);
    }

    @PostMapping("/messages")
    public Message addMessage(@RequestBody Message message) {
        return messageService.addMessage(message);
    }



}

package com.example.dockerpoc.dao;

import com.example.dockerpoc.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageDao extends JpaRepository<Message, Integer> {

}
